require('module-alias/register');

const express = require('express');
const colors = require('colors');
const app = express();
const server = require('http').createServer(app);

//database
let notes = {};

global.app = app;
global.server = server;

app.use(require("cors")({origin: "*"}));

app.get("/notes", async (req, res) => {
  const id = req.headers.userid;

  if (!notes[id])
    return res.json([]);
  return res.json(notes[id]);
});
app.post("/notes", async (req, res) => {
  const id = req.headers.userid;

  notes = {
    ...notes,
    [id]: [
      ...(notes[id] ?? []),
      {
        uid: Math.floor(Math.random() * 10000000000),
        title: req.body.title,
        content: req.body.content
      }
    ]
  }

  return res.json(notes[id]);
});
app.delete("/notes/:id", async (req, res) => {
  const id = req.headers.userid;
  const noteId = req.params.id;

  notes = {
    ...notes,
    [id]: notes[id].filter((note) => note.uid != noteId)
  }

  return res.json(notes[id]);
});
app.get("/", (req, res) => {
  res.send("Hello World3");
});

server.listen(3000, () => {
  //require("./core/helpers").showEmojiDataset("arrow");
  console.log("");
  console.log(colors.green(`--- Server listening on port ${3000} ---`));
  console.log("");
});

process.on('SIGTERM', () => {
  console.log('SIGTERM signal received: closing HTTP server')
  server.close(() => {
    console.log('Goodbye!')
  })
})
